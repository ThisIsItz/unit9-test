# UNIT9 React Test

This is a project made for UNIT9 for a recruitment task. The project is a prototype of an interactive menu burger.

## How to run
In the project directory, you can run `yarn start` or ` npm start ` to run the project.

This command opens [http://localhost:3000](http://localhost:3000) to view the project in the browser.

## What I used

The project was initilized using `npx create-react-app` to get a template.
The UI-library used was [Bootstrap](https://getbootstrap.com/).