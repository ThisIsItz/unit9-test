import React from 'react';
import './description.css';

const Description = ({description}) => {
    if(description === undefined){
        return <div className="description">Service</div>
    }else{
        return(
            <ul className="description showDescription">
                {description.map(element => (
                    <li className="des" key={element}>{element}</li>
                ))}
            </ul>
        )
    }
}

export default Description