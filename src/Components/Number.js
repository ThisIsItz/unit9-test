import React from 'react';
import './numbers.css';

const Number = ({number}) => {

    if(number === undefined){
        return (<div className="number col-2">00</div>)
    }else{
        return (<div className="number col-2 showNumber">{number}</div>)
    }

}

export default Number