import React from 'react'

const List = ({items, onElementHover, onElementOut}) => {
    
    return(
        <ul className="navbar-nav principal">
            {items.map(element => (
                <li className="nav-item" key={element.number} onMouseOut={() => onElementOut()} onMouseOver={() => {onElementHover(element)}}>
                    {element.title}
                </li>
            ))}
        </ul>
    )
}

export default List