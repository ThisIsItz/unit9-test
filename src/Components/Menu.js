import React from 'react';
import './menu.css';
import './button.css';
import Description from './Description';
import Number from './Number';
import List from './List';
import {ReactComponent as ReactIcon} from '../icon.svg';
import elements from '../elements';

class Menu extends React.Component {

    constructor(props){
        super(props)

        this.state={
            visible: false,
            shownElement: {}
        }

        this.toggleMenu = this.toggleMenu.bind(this)
        this.canvasRef = React.createRef();
    }

    drawImage(imagePath, {x, y}){
        const context = this.canvasRef.current.getContext('2d');
        
        const img = new Image()
        img.src = imagePath
        img.onload = () => {
            this.clearCavas()
            context.drawImage(img, x, y, 142, 120);
        }
    }

    clearCavas(){
        const canvas = this.canvasRef.current;
        const context = canvas.getContext('2d');
        
        context.clearRect(0, 0, canvas.width, canvas.height);
    }

    onMouseMove(evt) {
        if(this.state.shownElement.img){
            const height = this.canvasRef.current.clientHeight;
            const width = this.canvasRef.current.clientWidth;
            const pos = {
                x: ((evt.clientX-135)*600)/width,
                y: ((evt.clientY-87)*400)/height
            }
            this.drawImage(this.state.shownElement.img, pos);
        }
    }

    toggleMenu = () => {
        if(this.state.visible){
            this.setState({
                visible: false,
            })
        }else{
            this.setState({
                visible: true,
            })  
        }
    }

    onElementHover = (element) => {
        this.setState({shownElement: element})
    }

    onElementOut = () => {
        this.setState({shownElement: {}})
        this.clearCavas()
    }

    render(){
        const {visible, shownElement} = this.state
        return(
            <div id="menu"  onMouseMove={evt => this.onMouseMove(evt)}  className={`container-fluid row menu ${visible ? "" : "show"}`}>
                <canvas height="400" width="600" className={`canvas ${shownElement.number === undefined ? '' : 'canvasShow'}`} 
                    ref={this.canvasRef} id="canvas" />
                <div className="col-3 firstCol">
                    <Description description={shownElement.description}/>
                    <Number number={shownElement.number}/>
                </div>
                <div className="col-8 offset-1">
                    <button className={`button ${ visible ? 'hideButton' : 'showButton'}`} onClick={this.toggleMenu}>
                        <span className="icon"><ReactIcon /></span>
                    </button>
                    <List items={elements} onElementOut={this.onElementOut} onElementHover={this.onElementHover}/>
                </div>    
            </div>
        )
    }

}

export default Menu