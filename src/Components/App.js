import React from 'react';
import Menu from './Menu';
import './app.css';

function App() {
  return (
    <div className="background">
        <div className="welcomeText col-6 offset-5">Hello! I'm Juraj Molnár, designer and front-end developer. I</div>
        <Menu />
    </div>
  );
}

export default App;
