import cactus from './assets/cactus.jpg'
import yoga from './assets/yoga.jpg'
import computer from './assets/computer.jpg'
import girl from './assets/girl.jpg'
import bed from './assets/bed.jpg'

const elements = [
    {   title: "Helias",
        description: [
            "User Experience",
            "Web Design",
            "Interactive"
        ],
        number: "01",
        img: cactus,
    },
    {   title: "Hoboken Yogi",
        description: [
            "Art Direction",
            "User Experience",
            "Web Design",
            "Interactive",
            "Front-end"
        ],
        number: "02",
        img: yoga,
    },
    {   title: "Buzzworthy",
        description: [
            "Brand Strategy",
            "Brand Design",
            "Art Direction",
            "User Experience",
            "Web Design",
            "Interactive",
            "Front-end"
        ],
        number: "03",
        img: computer,
    },
    {   title: "Gatto",
        description: [
            "Art Direction",
            "User Experience",
            "Web Design"
        ],
        number: "04",
        img: girl,
    },
    {   title: "Snooze",
        description: [
            "Brand Strategy",
            "Brand Design",
            "Art Direction",
            "User Experience",
            "Web Design"
        ],
        number: "05",
        img: bed,
    }
] 

export default elements;